import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, NavigationEnd  } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-app';
  servicio: any;

  constructor(private http:HttpClient,public router: Router) 
  {
  
  }


  REST()
  {
    this.http.get("http://localhost:8180/helloworld-rs/rest/json").subscribe(snap => {
      this.servicio = snap;
      console.log(this.servicio);
      this.servicio=this.servicio.result;
    });
    
    
  }
}
